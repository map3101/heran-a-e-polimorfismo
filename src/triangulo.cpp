#include "triangulo.hpp"
#include <iostream>
#include <cmath>

triangulo::triangulo(){
	set_tipo("Triângulo");
	set_base(6.0f);
	set_altura(6.0f);
}


triangulo::triangulo(float base, float altura){
	set_tipo("Triângulo");
	set_base(base);
	set_altura(altura);
}


triangulo::triangulo(std::string tipo, float base, float altura){
	set_tipo(tipo);
	set_base(base);
	set_altura(altura);
}


triangulo::~triangulo(){}


float triangulo::calcula_area(){
	return (get_base() *get_altura())/2;
}

float triangulo::calcula_perimetro(){
	return sqrt(pow(get_base(),2)+pow(get_altura(),2))+get_base()+get_altura();
}
