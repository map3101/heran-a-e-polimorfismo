#include "pentagono.hpp"
#include <string>
#include <iostream>


pentagono::pentagono(){
	set_tipo("Pentágono");
	set_base(3.0f);
	set_altura(3.0f);
}


pentagono::pentagono(float base, float altura){
	set_tipo("Pentágono");
	set_base(base);
	set_altura(altura);
}


pentagono::pentagono(std::string tipo, float base, float altura){
	set_tipo(tipo);
	set_base(base);
	set_altura(altura);
}

pentagono::~pentagono(){}

float pentagono::calcula_area(){
	return ((get_base()*get_altura()) / 2) * 5;
}

float pentagono::calcula_perimetro(){
    return get_base()*5;
}
