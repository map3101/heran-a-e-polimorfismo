#include "quadrado.hpp"
#include <string>
#include <iostream>



quadrado::quadrado(){
	set_tipo("Quadrado");
	set_base(3.0f);
	set_altura(get_base());
}


quadrado::quadrado(float base, float altura){
	set_tipo("Quadrado");
	set_base(base);
	set_altura(altura);	
	if (get_base() != get_altura())
		throw(1);
}


quadrado::quadrado(std::string tipo, float base){
	set_tipo(tipo);
	set_base(base);
	set_altura(get_base());	
	if (get_base() != get_altura())
		throw(1);
}




quadrado::~quadrado(){}

