#include "triangulo.hpp"
#include "quadrado.hpp"
#include "paralelogramo.hpp"
#include "pentagono.hpp"
#include "hexagono.hpp"
#include "circulo.hpp"
#include "formageometrica.hpp"
#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main(){

	vector<FormaGeometrica *> lista;

	lista.push_back(new quadrado());
	lista.push_back(new triangulo());
	lista.push_back(new circulo());
	lista.push_back(new pentagono());
	lista.push_back(new paralelogramo());
	lista.push_back(new hexagono());

	for (FormaGeometrica * fig: lista){
		cout << "Tipo: " << fig->get_tipo() << endl;
		cout << "Área: " << fig->calcula_area() << endl;
		cout << "Perímetro: " << fig->calcula_perimetro() << endl;
	}



	return 0;
}

