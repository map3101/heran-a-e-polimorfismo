#include "paralelogramo.hpp"
#include <string>
#include <iostream>

paralelogramo::paralelogramo(){
	set_tipo("Paralelogramo");
	set_base(2.0f);
	set_altura(3.0f);
}


paralelogramo::paralelogramo(float base, float altura){
	set_tipo("Paralelogramo");
	set_base(base);
	set_altura(altura);
}


paralelogramo::paralelogramo(std::string tipo, float base, float altura){
	set_tipo(tipo);
	set_base(base);
	set_altura(altura);
}


paralelogramo::~paralelogramo(){}

float paralelogramo::calcula_area(){
	return get_base()*get_altura();
}

float paralelogramo::calcula_perimetro(){
	return 2*(get_base()+get_altura());
}
