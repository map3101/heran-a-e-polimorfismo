#include "hexagono.hpp"
#include <iostream>
#include <string>


hexagono::hexagono(){
	set_tipo("Hexágono");
	set_base(2.0f);
	set_altura(2.0f);
}

hexagono::hexagono(float base, float altura){
	set_tipo("Hexágono");
	set_base(base);
	set_altura(altura);
}

hexagono::hexagono(std::string tipo, float base, float altura){
	set_tipo(tipo);
	set_base(base);
	set_altura(altura);
}

hexagono::~hexagono(){}

float hexagono::calcula_area(){
	return ((get_altura()*get_base())/2)*6;
}

float hexagono::calcula_perimetro(){
	return get_base()*6;
}
