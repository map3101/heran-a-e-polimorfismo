#include "circulo.hpp"
#include <string>
#include <iostream>
#include <cmath>


circulo::circulo(){
	set_tipo("Circulo");
	set_base(2.5f);
}


circulo::circulo(float raio){
	set_tipo("Circulo");
	set_base(raio);
}


circulo::circulo(std::string tipo, float raio){
	set_tipo(tipo);
	set_base(raio);
}


float circulo::calcula_area(){
	return (pow(get_base(),2)*3.1415);
}

float circulo::calcula_perimetro(){
	return (2.0*3.1415*get_base());
}
