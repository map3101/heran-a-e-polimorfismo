#ifndef TRIANGULO_HPP
#define TRIANGULO_HPP
#include "formageometrica.hpp"
#include <string>
class triangulo : public FormaGeometrica
{



public:
triangulo();
~triangulo();
triangulo(float base, float altura);
triangulo(string tipo, float base, float altura);
float calcula_area();
float calcula_perimetro();

};

#endif
