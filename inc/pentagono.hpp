#ifndef PENTAGONO_HPP
#define PENTAGONO_HPP
#include "formageometrica.hpp"
#include <string>
class pentagono : public FormaGeometrica{

public:
pentagono();
pentagono(float base, float altura);
pentagono(std::string tipo, float base, float altura);
~pentagono();
float calcula_area();
float calcula_perimetro();

};

#endif
