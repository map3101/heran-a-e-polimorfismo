#ifndef CIRCULO_HPP
#define CIRCULO_HPP
#include "formageometrica.hpp"
#include <string>

using namespace std;

class circulo : public FormaGeometrica{
public:
circulo();
circulo(float raio);
circulo(string tipo, float raio);
~circulo();
float calcula_area();
float calcula_perimetro();
};

#endif
