#ifndef QUADRADO_HPP
#define QUADRADO_HPP
#include "formageometrica.hpp"
#include <string>
using namespace std;

class quadrado : public FormaGeometrica{

public:
quadrado();
quadrado(string tipo, float base);
quadrado(float base, float altura);
~quadrado();
float calcula_area;
float calcula_perimetro();
};

#endif
