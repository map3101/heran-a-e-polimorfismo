#ifndef HEXAGONO_HPP
#define HEXAGONO_HPP
#include "formageometrica.hpp"
#include <string>
class hexagono : public FormaGeometrica{

public:
hexagono();
hexagono(float base, float altura);
hexagono(std::string tipo, float base, float altura);
~hexagono();
float calcula_area();
float calcula_perimetro();
};

#endif
