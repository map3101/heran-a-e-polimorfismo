#ifndef PARALELOGRAMO_HPP
#define PARALELOGRAMO_HPP

#include "formageometrica.hpp"
#include <string>
class paralelogramo : public FormaGeometrica{

public: 
paralelogramo();
paralelogramo(float base, float altura);
paralelogramo(std::string tipo, float base, float altura);
~paralelogramo();
float calcula_area();
float calcula_perimetro();

};

#endif
